terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token     = "AQAAAAAISkpYAATuwYyzbq3DpEFejFA11jrYiVk"
  cloud_id  = "b1geqv1mf9rtifcrlvfc"
  folder_id = "b1g08e88u6k28bi766v7"
  zone      = "ru-central1-a"
}

resource "yandex_compute_instance" "web-app" {
  name = "reactjssrv"
  platform_id = "standard-v3"

  resources {
    core_fraction = 20
    cores  = 2
    memory = 2
  }

  scheduling_policy {
    preemptible = true
  }

  boot_disk {
    initialize_params {
      type = "network-hdd"
      size = 15
      image_id = "fd8egv6phshj1f64q94n"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-one.id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/skillbox_rsa.pub")}"
  }
}

resource "yandex_compute_instance" "web-nginx" {
  name = "nginxsrv"
  platform_id = "standard-v3"

  resources {
    core_fraction = 20
    cores  = 2
    memory = 2
  }

  scheduling_policy {
    preemptible = true
  }

  boot_disk {
    initialize_params {
      type = "network-hdd"
      size = 5
      image_id = "fd8egv6phshj1f64q94n"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-one.id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/skillbox_rsa.pub")}"
  }
}

resource "yandex_vpc_network" "network-one" {
  name = "network_one"
}

resource "yandex_vpc_subnet" "subnet-one" {
  name           = "subnet_one"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-one.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

output "nginx_servers_host" {
  value = yandex_compute_instance.web-nginx.network_interface.0.nat_ip_address
}

output "reactjs_servers_host" {
  value = yandex_compute_instance.web-app.network_interface.0.nat_ip_address
}

#output "internal_ip_reactapp" {
#  value = yandex_compute_instance.web-app.network_interface.0.ip_address
#}
